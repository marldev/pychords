"""Routines for mapping chord names (strings) to numerical representations.


Codes should enable the easy calculation of a variety of metrics:

- accuracy + criteria
    root + quality + bass
- quality edit distance + weighted
- note edit distance + weighted


To achieve this, a chord label gets mapped to four different pieces of
information. This is best explained via example:

Consider the chord label 'F#:hdim7(*b3,4)/b7', which consists of the following
parts:
- root: F#
- quality: hdim7
- extensions: (*b3, 4)
- bass: b7

In a coded format, it will yield the following:
- root: 6
- quality_map: [1,0,0,1,0,0,1,0,0,0,1,0]
- note_map: [1,0,0,0,0,1,1,0,0,0,1,0]
- bass: 10


"""
import numpy as np

from label import split
from core import NO_CHORD
from core import pitch_class_to_semitone
from core import scale_degree_to_bitmap
from core import scale_degree_to_semitone
from core import quality_to_bitmap


def encode(chord, extended_chords=False):
    """
    Parameters
    ----------
    chord: str
        Chord label to encode.
    reduce_extended_chords: bool, default=True
        Map the upper voicings of extended chords (9's, 11's, 13's) to semitone
        extensions.

    Returns
    -------
    root_number: int
        Semitone of the chord's root.
    quality_bitmap: np.ndarray of ints, in [0, 1]
        12-dim vector of semitones in the given chord quality.
    note_bitmap: np.ndarray of ints, in [0, 1]
        12-dim vector of specific semitones in the given chord spelling.
    bass_number: int
        Relative semitone of the chord's bass note, e.g. 0=root, 7=fifth, etc.
    """
    if chord == NO_CHORD:
        return -1, np.array([0]*12), np.array([0]*12), -1
    root, quality, exts, bass = split(chord, extended_chords=extended_chords)
    root_number = pitch_class_to_semitone(root)
    bass_number = scale_degree_to_semitone(bass)

    quality_bitmap = quality_to_bitmap(quality)
    note_bitmap = np.array(quality_bitmap)
    for sd in list(exts):
        note_bitmap += scale_degree_to_bitmap(sd)

    note_bitmap = (note_bitmap > 0).astype(np.int)
    return root_number, quality_bitmap, note_bitmap, bass_number


def encode_many(chords, extended_chords=False):
    """
    Parameters
    ----------
    chord: str
        Chord label to encode.
    reduce_extended_chords: bool, default=True
        Map the upper voicings of extended chords (9's, 11's, 13's) to semitone
        extensions.

    Returns
    -------
    root_number: np.ndarray of ints
        Semitone of the chord's root.
    quality_bitmap: np.ndarray of ints, in [0, 1]
        12-dim vector of relative semitones in the given chord quality.
    note_bitmap: np.ndarray of ints, in [0, 1]
        12-dim vector of relative semitones in the given chord spelling.
    bass_number: np.ndarray
        Relative semitones of the chord's bass notes.
    """
    num_items = len(chords)
    roots, basses = np.zeros([2, num_items], dtype=np.int)
    qualities, notes = np.zeros([2, num_items, 12], dtype=np.int)
    for i, c in enumerate(chords):
        roots[i], qualities[i], notes[i], basses[i] = encode(
            c, extended_chords)
    return roots, qualities, notes, basses


def decode(root_number, quality_bitmap, note_bitmap, bass_number):
    """
    root_number: int
    quality_bitmap: np.ndarray
    note_bitmap: np.ndarray
    bass_number: int
    """
    raise NotImplementedError(
        "Haven't gotten here yet, not sure it's necessary.")
