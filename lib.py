r'''Functions and other supporting code for wrangling chord labels.


Conventions:
-------------------------------
- Pitch class counting starts at C, e.g. C: 0, D:2, E:4, F:5, etc.


Defintions:
-------------------------------
- chord label: String representation of a chord name, e.g. "G:maj(4)/5"
- scale degree: String representation of a diatonic interval, relative to the
    root note, e.g. 'b6', '#5', or '7'
- bass interval: String representation of the bass note's scale degree.
- bitmap: Positional binary vector indicating active pitch classes; may be
    absolute or relative depending on context in the code.

'''

import numpy as np

BITMAP_LENGTH = 12
NO_CHORD = "N"
NO_CHORD_ENCODED = -1, np.array([0]*BITMAP_LENGTH), -1
X_CHORD = "X"
X_CHORD_ENCODED = -1, np.array([-1]*BITMAP_LENGTH), -1
# See Line 445
BASS_INTERVALS = ['strict', 'include', 'ignore'][1]


class InvalidChordException(BaseException):
    r'''Exception class for suspect / invalid chord labels.'''

    def __init__(self, message='', chord_label=None):
        self.message = message
        self.chord_label = chord_label
        self.name = self.__class__.__name__


# --- Chord Primitives ---
def _pitch_classes():
    r'''Map from pitch class (str) to semitone (int).'''
    pitch_classes = ['C', 'D', 'E', 'F', 'G', 'A', 'B']
    semitones = [0, 2, 4, 5, 7, 9, 11]
    return dict([(c, s) for c, s in zip(pitch_classes, semitones)])


def _scale_degrees():
    r'''Mapping from scale degrees (str) to semitones (int).'''
    degrees = ['1', '2', '3', '4', '5', '6', '7', '9', '10', '11', '12', '13']
    semitones = [0, 2, 4, 5, 7, 9, 11, 14, 16, 17, 19, 21]
    return dict([(d, s) for d, s in zip(degrees, semitones)])


# Maps pitch classes (strings) to semitone indexes (ints).
PITCH_CLASSES = _pitch_classes()


def pitch_class_to_semitone(pitch_class):
    r'''Convert a pitch class to semitone.

    Parameters
    ----------
    pitch_class: str
        Spelling of a given pitch class, e.g. 'C#', 'Gbb'

    Returns
    -------
    semitone: int
        Semitone value of the pitch class.

    Raises
    ------
    - InvalidChordException
    '''
    semitone = 0
    for idx, char in enumerate(pitch_class):
        if char == '#' and idx > 0:
            semitone += 1
        elif char == 'b' and idx > 0:
            semitone -= 1
        elif idx == 0:
            semitone = PITCH_CLASSES.get(char)
        else:
            raise InvalidChordException(
                "Pitch class improperly formed: %s" % pitch_class)
    return semitone % 12


# Maps scale degrees (strings) to semitone indexes (ints).
SCALE_DEGREES = _scale_degrees()


def scale_degree_to_semitone(scale_degree):
    r'''Convert a scale degree to semitone.

    Parameters
    ----------
    scale degree: str
        Spelling of a relative scale degree, e.g. 'b3', '7', '#5'

    Returns
    -------
    semitone: int
        Relative semitone of the scale degree, wrapped to a single octave

    Raises
    ------
    InvalidChordException
    '''
    semitone = 0
    offset = 0
    if scale_degree.startswith("#"):
        offset = scale_degree.count("#")
        scale_degree = scale_degree.strip("#")
    elif scale_degree.startswith('b'):
        offset = -1 * scale_degree.count("b")
        scale_degree = scale_degree.strip("b")

    semitone = SCALE_DEGREES.get(scale_degree, None)
    if semitone is None:
        raise InvalidChordException(
            "Scale degree improperly formed: %s" % scale_degree)
    return semitone + offset


def scale_degree_to_bitmap(scale_degree):
    '''Create a bitmap representation of a scale degree.

    Note that values in the bitmap may be negative, indicating that the
    semitone is to be removed.

    Parameters
    ----------
    scale_degree: str
        Spelling of a relative scale degree, e.g. 'b3', '7', '#5'

    Returns
    -------
    bitmap: np.ndarray, in [-1, 0, 1]
        Bitmap representation of this scale degree (12-dim).
    '''
    sign = 1
    if scale_degree.startswith("*"):
        sign = -1
        scale_degree = scale_degree.strip("*")
    edit_map = [0] * BITMAP_LENGTH
    sd_idx = scale_degree_to_semitone(scale_degree)
    if sd_idx < BITMAP_LENGTH:
        edit_map[sd_idx % BITMAP_LENGTH] = sign
    return np.array(edit_map)


# Maps quality strings to bitmaps, corresponding to relative pitch class
# semitones, i.e. vector[0] is the tonic.
QUALITIES = {
    #           1     2     3     4  5     6     7
    'maj':     [1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0],
    'min':     [1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0],
    'aug':     [1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0],
    'dim':     [1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0],
    'sus4':    [1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0],
    'sus2':    [1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0],
    '7':       [1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0],
    'maj7':    [1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1],
    'min7':    [1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0],
    'minmaj7': [1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1],
    'maj6':    [1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0],
    'min6':    [1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0],
    'dim7':    [1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0],
    'hdim7':   [1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0],
    'maj9':    [1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1],
    'min9':    [1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0],
    '9':       [1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0],
    'b9':      [1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0],
    '#9':      [1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0],
    'min11':   [1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0],
    '11':      [1, 0, 0, 0, 1, 1, 0, 1, 0, 0, 1, 0],
    '#11':     [1, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0],
    'maj13':   [1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1],
    'min13':   [1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0],
    '13':      [1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0],
    'b13':     [1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0],
    '1':       [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    '5':       [1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
    '':        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]}


def quality_to_chroma(quality):
    '''Return the chroma bitmap for a given quality.

    Parameters
    ----------
    quality: str
        Chord quality name.

    Returns
    -------
    chroma: np.ndarray, in [0, 1]
        Chroma representation of this quality (12-dim).

    Raises
    ------
    InvalidChordException
    '''
    if not quality in QUALITIES:
        raise InvalidChordException(
            "Unsupported chord quality shorthand: '%s'." % quality)
    return np.array(QUALITIES[quality])


# --- Chord Label Parsing ---
def validate_chord_label(chord_label):
    '''Test for well-formedness of a chord label.

    Parameters
    ----------
    chord: str
        Chord label to validate.

    Raises
    ------
    InvalidFormatException
    '''
    # Test for single special characters
    for one_char in [':', '/', '(', ')']:
        if chord_label.count(one_char) > 1:
            raise InvalidChordException(
                "Chord label may only contain one '%s'. "
                "Received: '%s'" % (one_char, chord_label))

    # Test for closed parens
    parens = [paren in chord_label for paren in ['(', ')']]
    if any(parens) and not all(parens):
        raise InvalidChordException(
            "Chord label must have closed parentheses. "
            "Received: '%s'" % chord_label)


def split(chord_label):
    '''Parse a chord label into its four constituent parts:
    - root
    - quality shorthand
    - scale degrees
    - bass

    Note: Chords lacking quality AND interval information are major.
      If a quality is specified, it is returned.
      If an interval is specified WITHOUT a quality, the quality field is
        empty.

    Some examples:
        'C' -> ['C', 'maj', {}, '1']
        'G#:min(*b3,*5)/5' -> ['G#', 'min', {'*b3', '*5'}, '5']
        'A:(3)/6' -> ['A', '', {'3'}, '6']


    Parameters
    ----------
    chord_label: str
        A chord label.

    Returns
    -------
    chord_parts: list
        Split version of the chord label.
    '''
    chord_label = str(chord_label)
    validate_chord_label(chord_label)
    if chord_label == NO_CHORD:
        return [chord_label, '', set(), '']

    bass = '1'
    if "/" in chord_label:
        chord_label, bass = chord_label.split("/")

    scale_degrees = set()
    omission = False
    if "(" in chord_label:
        chord_label, scale_degrees = chord_label.split("(")
        omission = "*" in scale_degrees
        scale_degrees = scale_degrees.strip(")")
        scale_degrees = set([i.strip() for i in scale_degrees.split(",")])

    # Note: Chords lacking quality AND added interval information are major.
    #   If a quality shorthand is specified, it is returned.
    #   If an interval is specified WITHOUT a quality, the quality field is
    #     empty.
    #   Intervals specifying omissions MUST have a quality.
    if omission and not ":" in chord_label:
        raise InvalidChordException(
            "Intervals specifying omissions MUST have a quality.")
    quality = '' if scale_degrees else 'maj'
    if ":" in chord_label:
        root, quality_name = chord_label.split(":")
        # Extended chords (with ":"s) may not explicitly have Major qualities,
        # so only overwrite the default if the string is not empty.
        if quality_name:
            quality = quality_name.lower()
    else:
        root = chord_label

    return [root, quality, scale_degrees, bass]


def join(root, quality='', extensions=None, bass=''):
    '''Join the parts of a chord into a complete chord label.

    Parameters
    ----------
    root: str
        Root pitch class of the chord, e.g. 'C', 'Eb'
    quality: str
        Quality of the chord, e.g. 'maj', 'hdim7'
    extensions: list
        Any added or absent scaled degrees for this chord, e.g. ['4', '*3']
    bass: str
        Scale degree of the bass note, e.g. '5'.

    Returns
    -------
    chord_label: str
        A complete chord label.

    Raises
    ------
    InvalidChordException: Thrown if the provided args yield a garbage chord
        label.
    '''
    chord_label = root
    if quality or extensions:
        chord_label += ":%s" % quality
    if extensions:
        chord_label += "(%s)" % ",".join(extensions)
    if bass:
        chord_label += "/%s" % bass
    validate_chord_label(chord_label)
    return chord_label


# --- Chords to Numerical Representations ---
def encode(chord_label):
    """Translate a chord label to numerical representations for evaluation.

    Parameters
    ----------
    chord_label: str
        Chord label to encode.

    Returns
    -------
    root_number: int
        Absolute semitone of the chord's root.
    semitone_bitmap: np.ndarray of ints, in [0, 1]
        12-dim vector of relative semitones in the chord spelling.
    bass_number: int
        Relative semitone of the chord's bass note, e.g. 0=root, 7=fifth, etc.

    Raises
    ------
    InvalidChordException: Thrown if the given bass note is not explicitly
        named as an extension.
    """

    if chord_label == NO_CHORD:
        return NO_CHORD_ENCODED
    if chord_label == X_CHORD:
        return X_CHORD_ENCODED
    root, quality, scale_degrees, bass = split(chord_label)

    root_number = pitch_class_to_semitone(root)
    bass_number = scale_degree_to_semitone(bass) % 12

    semitone_bitmap = quality_to_chroma(quality)
    semitone_bitmap[0] = 1

    for scale_degree in scale_degrees:
        semitone_bitmap += scale_degree_to_bitmap(scale_degree)

    semitone_bitmap = (semitone_bitmap > 0).astype(np.int)
    if not semitone_bitmap[bass_number] and BASS_INTERVALS == 'strict':
        raise InvalidChordException(
            "Given bass scale degree is absent from this chord: "
            "%s" % chord_label, chord_label)
    elif BASS_INTERVALS == 'include':
        semitone_bitmap[bass_number] = 1
    return root_number, semitone_bitmap, bass_number


def encode_many(chord_labels):
    """Translate a set of chord labels to numerical representations for sane
    evaluation.

    Parameters
    ----------
    chord_labels: list
        Set of chord labels to encode.
    reduce_extended_chords: bool, default=True
        Map the upper voicings of extended chords (9's, 11's, 13's) to semitone
        extensions.

    Returns
    -------
    root_number: np.ndarray of ints
        Absolute semitone of the chord's root.
    interval_bitmap: np.ndarray of ints, in [0, 1]
        12-dim vector of relative semitones in the given chord quality.
    bass_number: np.ndarray
        Relative semitones of the chord's bass notes.

    Raises
    ------
    InvalidChordException: Thrown if the given bass note is not explicitly
        named as an extension.
    """
    num_items = len(chord_labels)
    roots, basses = np.zeros([2, num_items], dtype=np.int)
    semitones = np.zeros([num_items, 12], dtype=np.int)
    local_cache = dict()
    for i, label in enumerate(chord_labels):
        result = local_cache.get(label, None)
        if result is None:
            result = encode(label)
            local_cache[label] = result
        roots[i], semitones[i], basses[i] = result
    return roots, semitones, basses


def decode(root_number,  semitone_bitmap, bass_number):
    """Translate a numerical representations into a chord label.

    Note that this is not perfectly invertible with 'encode'.

    Parameters
    ----------
    root_number: int
        Absolute semitone of the chord's root.
    semitone_bitmap: np.ndarray of ints, in [0, 1]
        12-dim vector of relative semitones in the chord spelling.
    bass_number: int
        Relative semitone of the chord's bass note, e.g. 0=root, 7=fifth, etc.

    Returns
    -------
    chord_label: str
        Resulting chord label.

    Raises
    ------
    - InvalidChordException: Thrown if any of the given parts are invalid.
    """
    pass


def rotate_bitmap_to_root(bitmap, root):
    '''Circularly shift a relative bitmap to its asbolute pitch classes.

    For clarity, the best explanation is an example. Given 'G:Maj', the root
    and quality map are as follows:
        root=5
        quality=[1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0]  # Relative chord shape

    After rotating to the root, the resulting bitmap becomes:
        chroma = [0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1]  # G, B, and D

    Parameters
    ----------
    bitmap: np.ndarray, shape=(12,)
        Bitmap of active notes, relative to the given root.

    root: int
        Absolute pitch class number.

    Returns
    -------
    chroma: np.ndarray, shape=(12,)
        Active pitch classes (chroma).
    '''
    bitmap = np.asarray(bitmap)
    assert bitmap.ndim == 1, "Currently only 1D bitmaps are supported."
    idxs = list(np.nonzero(bitmap))
    idxs[-1] = (idxs[-1] + root) % 12
    abs_bitmap = np.zeros_like(bitmap)
    abs_bitmap[idxs] = 1
    return abs_bitmap


def rotate_bitmaps_to_roots(bitmaps, roots):
    '''Circularly shift a relative bitmaps to asbolute pitch classes.

    See rotate_bitmap_to_root for more information.

    Parameters
    ----------
    bitmap: np.ndarray, shape=(N, 12)
        Bitmap of active notes, relative to the given root.

    root: np.ndarray, shape=(N,)
        Absolute pitch class number.

    Returns
    -------
    bitmap: np.ndarray, shape=(N, 12)
        Absolute bitmaps of active pitch classes.
    '''
    chromas = []
    for bitmap, root in zip(bitmaps, roots):
        chromas.append(rotate_bitmap_to_root(bitmap, root))
    return np.asarray(chromas)


def rotate_bass_to_root(bass, root):
    '''Rotate a relative bass interval to its asbolute pitch class.

    Parameters
    ----------
    bass: int
        Relative bass interval.
    root: int
        Absolute root pitch class.

    Returns
    -------
    bass: int
        Pitch class of the bass intervalself.
    '''
    return (bass + root) % 12


def chord_to_chroma(chord_label):
    root, semitones, bass = encode(chord_label)
    return rotate_bitmap_to_root(semitones, root)


def _generate_tonnetz_matrix(radii):
    """Return a Tonnetz transform matrix.

    Parameters
    ----------
    radii: array_like, shape=(3,)
        Desired radii for each harmonic subspace (fifths, maj-thirds,
        min-thirds).

    Returns
    -------
    phi: np.ndarray, shape=(12,6)
        Bases for transforming a chroma matrix into tonnetz coordinates.
    """
    assert len(radii) == 3
    basis = []
    for l in range(12):
        basis.append([
            radii[0]*np.sin(l*7*np.pi/6), radii[0]*np.cos(l*7*np.pi/6),
            radii[1]*np.sin(l*3*np.pi/2), radii[1]*np.cos(l*3*np.pi/2),
            radii[2]*np.sin(l*2*np.pi/3), radii[2]*np.cos(l*2*np.pi/3)])
    return np.array(basis)


def chord_to_tonnetz(chord_label, radii=(1.0, 1.0, 0.5)):
    """Return a Tonnetz coordinates for a given chord label.

    Parameters
    ----------
    chord_label: str
        Chord label to transform.
    radii: array_like, shape=(3,), default=(1.0, 1.0, 0.5)
        Desired radii for each harmonic subspace (fifths, maj-thirds,
        min-thirds). Default based on E. Chew's spiral model.

    Returns
    -------
    tonnnetz: np.ndarray, shape=(6,)
        Coordinates in tonnetz space for the given chord label.
    """
    chroma = chord_to_chroma(chord_label)
    phi = _generate_tonnetz_matrix(radii)
    tonnetz = np.dot(chroma, phi)
    scalar = 1 if np.sum(chroma) == 0 else np.sum(chroma)
    return tonnetz / scalar
