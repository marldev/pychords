"""Routines for managing guitar chord shapes.

Note: The concept of 'off' is represented numerically by -1. This should allow
for graceful wrap-around in bitmap representations (off is the last index).
"""

import numpy as np

#       Strings    E2  A2  D3  G3  B3  E4
STANDARD_TUNING = [40, 45, 50, 55, 59, 64]
OFF_CHAR = 'X'
NO_CHORD = ','.join([OFF_CHAR] * 6)


def decode(fret_label, delimiter=',', off_char=OFF_CHAR):
    frets = []
    for x in fret_label.split(delimiter):
        x = x.strip("P")
        frets.append(-1 if x == off_char else int(x))
    return frets


def frets_to_chroma(frets):
    """
    Parameters
    ----------
    frets: array_like
        Numerical representation of the fretboard.

    Returns
    -------
    chroma: np.ndarray, shape=(12,)
        Chroma bitvector of active pitch classes.
    """
    chroma = np.zeros(12, dtype=int)
    for x, s in zip(frets, STANDARD_TUNING):
        if x >= 0:
            chroma[(x + s) % 12] = 1
    return chroma
