"""Chord label routines.

Some definitions:

The name of a chord, in its string representation (e.g. 'C:maj7(4)/5') is
referred to as its 'label', which will consist of at most four parts:
 - root
 - quality
 - extensions
 - bass notes

For the sake of convenience and reducing the space of chord labels, which
adheres to a power law distribution, it is not uncommon to map extended chords
(such as 9's, 11's, and 13's) to a smaller subset by preserving the primary
quality and adding the higher voices (the 2, 4, and 6th degrees, respectively)
to the set of chord extensions.

"""

from pychords.core import NO_CHORD
from pychords.core import reduce_extended_quality
from pychords.core import InvalidFormatException


def _validate(chord):
    """Test for well-formedness of a chord label.

    Parameters
    ----------
    chord: str
        Chord label to validate.

    Raises:
    -------
    InvalidFormatException
    """

    # Test for single special characters
    for one_char in [':', '/', '(', ')']:
        if chord.count(one_char) > 1:
            raise InvalidFormatException(
                "Chord label may only contain one '%s'. "
                "Received: '%s'" % (one_char, chord))

    # Test for closed parens
    parens = [paren in chord for paren in ['(', ')']]
    if any(parens) and not all(parens):
        raise InvalidFormatException(
            "Chord label must have closed parentheses. "
            "Received: '%s'" % chord)


def split(chord, extended_chords=False):
    """Parse a chord label into its four constituent parts:
    - root
    - quality
    - bass
    - extensions

    Some examples:
        'C' -> ['C', 'maj', '', []]
        'G#:min(*b3,*5)/5' -> ['G#', 'min', '5', ['*b3', '*5']]


    Parameters:
    -----------
    chord: str
        A chord label.

    Returns
    -------
    chord_parts: list
        Unpacked version of the chord label.

    """
    chord = str(chord)
    _validate(chord)
    if chord == NO_CHORD:
        return [chord, '', set(), '']

    bass = '1'
    if "/" in chord:
        chord, bass = chord.split("/")

    extensions = set()
    if "(" in chord:
        chord, extensions = chord.split("(")
        extensions = extensions.strip(")")
        extensions = set([e.strip() for e in extensions.split(",")])

    # By default, unspecified qualities are major.
    quality = 'maj'
    if ":" in chord:
        root, quality_name = chord.split(":")
        # Extended chords (with ":"s) may not explicitly have Major qualities,
        # so only overwrite the default if the string is not empty.
        if quality_name:
            quality = quality_name.lower()
    else:
        root = chord

    if not extended_chords:
        quality, addl_extensions = reduce_extended_quality(quality)
        extensions.update(addl_extensions)

    return [root, quality, extensions, bass]


def join(root, quality='', extensions=None, bass=''):
    """Join the parts of a chord into a complete chord label.

    Parameters
    ----------
    root: str
        Root pitch class of the chord, e.g. 'C', 'Eb'
    quality: str
        Quality of the chord, e.g. 'maj', 'hdim7'
    extensions: list
        Any added or absent scaled degrees for this chord, e.g. ['4', '*3']
    bass: str
        Scale degree of the bass note, e.g. '5'.

    Returns
    -------
    label: str
        A complete chord label.
    """
    chord = root
    if quality or extensions:
        chord += ":%s" % quality
    if extensions:
        chord += "(%s)" % ",".join(extensions)
    if bass:
        chord += "/%s" % bass
    _validate(chord)
    return chord

# TODO(ejhumphrey): Figure out what to do with this.
# def reduce_extended_chord(chord):
#     """
#     """
#     root, quality, exts, bass = split(chord)
#     # Optionally, if this is an extended quality and we want to map it down to
#     # the predefined subset, do so now.
#     quality, addl_extensions = reduce_extended_quality(quality)
#     exts.update(addl_extensions)
#     return join(root, quality, exts, bass)
