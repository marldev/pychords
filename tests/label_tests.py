"""
"""

import unittest

from pychords import label
from pychords.core import InvalidFormatException


class LabelTests(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_well_formedness(self):
        # Good chords should pass.
        for chord in ['C', 'Eb:min/5', 'A#:dim7', 'B:maj(*1,*5)/3', 'A#:sus4']:
            label._validate(chord)

        # Bad chords should fail.
        self.assertRaises(
            InvalidFormatException, label._validate, "C::maj")
        self.assertRaises(
            InvalidFormatException, label._validate, "C//5")
        self.assertRaises(
            InvalidFormatException, label._validate, "C((4)")
        self.assertRaises(
            InvalidFormatException, label._validate, "C5))")
        self.assertRaises(
            InvalidFormatException, label._validate, "C:maj(*3/3")
        self.assertRaises(
            InvalidFormatException, label._validate, "Cmaj*3/3)")

    def test_split(self):
        self.assertEqual(label.split('C'), ['C', 'maj', set(), '1'])
        self.assertEqual(label.split('B:maj(*1,*3)/5'),
                         ['B', 'maj', set(['*1', '*3']), '5'])
        self.assertEqual(label.split('Ab:min/b3'), ['Ab', 'min', set(), 'b3'])
        self.assertEqual(label.split('N'), ['N', '', set(), ''])

    def test_join(self):
        self.assertEqual(label.join('F#'), 'F#')
        self.assertEqual(label.join('F#', quality='hdim7'), 'F#:hdim7')
        self.assertEqual(
            label.join('F#', extensions={'*b3', '4'}), 'F#:(*b3,4)')
        self.assertEqual(label.join('F#', bass='b7'), 'F#/b7')
        self.assertEqual(label.join('F#', extensions={'*b3', '4'}, bass='b7'),
                         'F#:(*b3,4)/b7')
        self.assertEqual(label.join('F#', quality='hdim7', bass='b7'),
                         'F#:hdim7/b7')
        self.assertEqual(label.join('F#', 'hdim7', {'*b3', '4'}, 'b7'),
                         'F#:hdim7(*b3,4)/b7')

    # def test_reduce_extended_chord(self):
    #     self.assertEqual(label.reduce_extended_chord('A:9'), 'A:7(9)')
    #     self.assertEqual(label.reduce_extended_chord('G:11'), 'G:7(9,11)')
    #     # TODO(ejhumphrey): This is super unstable (merging sets).
    #     self.assertEqual(label.reduce_extended_chord('G:11(4)'), 'G:7(9,11,4)')


if __name__ == "__main__":
    unittest.main()
