"""
"""

import unittest

from pychords import core


class CoreTests(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_pitch_class_to_semitone(self):
        self.assertEqual(core.pitch_class_to_semitone('Gbb'), 5)
        self.assertEqual(core.pitch_class_to_semitone('G'), 7)
        self.assertEqual(core.pitch_class_to_semitone('G#'), 8)
        self.assertEqual(core.pitch_class_to_semitone('Cb'), 11)
        self.assertEqual(core.pitch_class_to_semitone('B#'), 0)

        self.assertRaises(
            core.InvalidFormatException,
            core.pitch_class_to_semitone, "Cab")

        self.assertRaises(
            core.InvalidFormatException,
            core.pitch_class_to_semitone, "#C")

        self.assertRaises(
            core.InvalidFormatException,
            core.pitch_class_to_semitone, "bG")

    def test_scale_degree_to_semitone(self):
        self.assertEqual(core.scale_degree_to_semitone('b7'), 10)
        self.assertEqual(core.scale_degree_to_semitone('#3'), 5)
        self.assertEqual(core.scale_degree_to_semitone('1'), 0)
        self.assertEqual(core.scale_degree_to_semitone('b1'), 11)
        self.assertEqual(core.scale_degree_to_semitone('#7'), 0)
        self.assertEqual(core.scale_degree_to_semitone('bb5'), 5)

        self.assertRaises(
            core.InvalidFormatException,
            core.scale_degree_to_semitone, "7b")

        self.assertRaises(
            core.InvalidFormatException,
            core.scale_degree_to_semitone, "4#")

        self.assertRaises(
            core.InvalidFormatException,
            core.scale_degree_to_semitone, "77")


if __name__ == "__main__":
    unittest.main()
