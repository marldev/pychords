"""pychords

Modules
-------
- bitcode.py : Translate chords into 32-bit code vectors.
- parser.py : Unpack chord names into stable strings.

"""
# Import all submodules (for each task)
from . import core
from . import numeric
from . import label

__version__ = '0.0.1'
