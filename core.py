"""Chord Definitions -- because some things just need to be hard-coded.

Note that pitch class counting starts at C, e.g.
     C: 0, D:2, E:4, F:5, ...
"""
import numpy as np

NO_CHORD = "N"


class InvalidFormatException(BaseException):
    """Hollow class for invalid formatting."""
    pass


def _pitch_classes():
    """Map from pitch class (str) to semitone (int)."""
    pitch_classes = ['C', 'D', 'E', 'F', 'G', 'A', 'B']
    semitones = [0, 2, 4, 5, 7, 9, 11]
    return dict([(c, s) for c, s in zip(pitch_classes, semitones)])


def _scale_degrees():
    """Mapping from scale degrees (str) to semitones (int)."""
    degrees = ['1', '2', '3', '4', '5', '6', '7', '9', '10', '11', '12', '13']
    semitones = [0, 2, 4, 5, 7, 9, 11, 2, 4, 5, 7, 9]
    return dict([(d, s) for d, s in zip(degrees, semitones)])


# Maps pitch classes (strings) to semitone indexes (ints).
PITCH_CLASSES = _pitch_classes()


def pitch_class_to_semitone(pitch_class):
    """Convert a pitch class to semitone.

    Parameters
    ----------
    pitch_class: str
        Spelling of a given pitch class, e.g. 'C#', 'Gbb'

    Returns
    -------
    semitone: int
        Semitone value of the pitch class.

    Raises
    ------
    InvalidFormatException
    """
    semitone = 0
    for idx, char in enumerate(pitch_class):
        if char == '#' and idx > 0:
            semitone += 1
        elif char == 'b' and idx > 0:
            semitone -= 1
        elif idx == 0:
            semitone = PITCH_CLASSES.get(char)
        else:
            raise InvalidFormatException(
                "Pitch class improperly formed: %s" % pitch_class)
    return semitone % 12


# Maps scale degrees (strings) to semitone indexes (ints).
SCALE_DEGREES = _scale_degrees()


def scale_degree_to_semitone(scale_degree):
    """Convert a scale degree to semitone.

    Parameters
    ----------
    scale degree: str
        Spelling of a relative scale degree, e.g. 'b3', '7', '#5'

    Returns
    -------
    semitone: int
        Relative semitone value of the scale degree.

    Raises
    ------
    InvalidFormatException
    """
    semitone = 0
    offset = 0
    if scale_degree.startswith("#"):
        offset = scale_degree.count("#")
        scale_degree = scale_degree.strip("#")
    elif scale_degree.startswith('b'):
        offset = -1 * scale_degree.count("b")
        scale_degree = scale_degree.strip("b")

    semitone = SCALE_DEGREES.get(scale_degree, None)
    if semitone is None:
        raise InvalidFormatException(
            "Scale degree improperly formed: %s" % scale_degree)
    return (semitone + offset) % 12


def scale_degree_to_bitmap(scale_degree):
    """
    """
    sign = 1
    if scale_degree.startswith("*"):
        sign = -1
        scale_degree = scale_degree.strip("*")
    edit_map = [0] * 12
    edit_map[scale_degree_to_semitone(scale_degree)] = sign
    return np.array(edit_map)


# Maps quality strings to bit vectors, corresponding to pitch class semitones.
QUALITIES = {
    'maj':   [1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0],
    'min':   [1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0],
    'aug':   [1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0],
    'dim':   [1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0],
    'sus4':  [1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0],
    'sus2':  [1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0],
    '7':     [1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0],
    'maj7':  [1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1],
    'min7':  [1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0],
    'maj6':  [1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0],
    'min6':  [1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0],
    'dim7':  [1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0],
    'hdim7': [1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0]}


def quality_to_bitmap(quality):
    """Return the note map for the given quality.

    Parameters
    ----------
    quality: str
        Quality to code.

    Returns
    -------
    bitmap: list of ints, in [0, 1]
        Bitmap representation of this quality (12-dim).

    Raises
    ------
    InvalidFormatException
    """
    if not quality in QUALITIES:
        raise InvalidFormatException(
            "Unsupported chord quality: '%s' "
            "Did you mean to reduce extended chords?" % quality)
    return np.array(QUALITIES[quality])


# Maps extended chord qualities to the subset above, translating additional
# voicings to extensions as a set of scale degrees (strings).
EXTENDED_QUALITY_REDUX = {
    'minmaj7': ('min',  set(['7'])),
    'maj9':    ('maj7', set(['9'])),
    'min9':    ('min7', set(['9'])),
    '9':       ('7',    set(['9'])),
    'b9':      ('7',    set(['b9'])),
    '#9':      ('7',    set(['#9'])),
    '11':      ('7',    set(['9', '11'])),
    '#11':     ('7',    set(['9', '#11'])),
    '13':      ('7',    set(['9', '11', '13'])),
    'b13':     ('7',    set(['9', '11', 'b13'])),
    'min11':   ('min7', set(['9', '11'])),
    'maj13':   ('maj7', set(['9', '11', '13'])),
    'min13':   ('min7', set(['9', '11', '13']))}


def reduce_extended_quality(quality):
    """Map an extended chord quality to a simpler one, moving upper voices to
    a set of scale degree extensions.

    Parameters
    ----------
    quality: str
        Extended chord quality to reduce.

    Returns
    -------
    base_quality: str
        New chord quality.
    extensions: set
        Scale degrees extensions for the quality.
    """
    # if not quality in EXTENDED_QUALITY_REDUX:
    #     raise InvalidFormatException(
    #         "Not an extended chord quality: '%s' "
    #         "Perhaps it is already reduced?" % quality)
    return EXTENDED_QUALITY_REDUX.get(quality, (quality, set()))
